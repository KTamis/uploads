<?php

// src/Routes/web.php
Route::group(['middleware' => ['web']], function () {
    // first variant
    Route::get('/tracks-upload/{config}/{id}', 'Tamis\Upload\Controllers\UploadController@upload');
    Route::post('/tracks-store', 'Tamis\Upload\Controllers\UploadController@store');
    
    // second variant
    Route::get('/arts-upload/{config}/{id}', 'Tamis\Upload\Controllers\UploadController@upload3');
    Route::post('/arts-store', 'Tamis\Upload\Controllers\UploadController@storeArts');
    
    // third variant
    Route::get('/items-upload/{config}/{parent_id}/{sub_parent_id}', 'Tamis\Upload\Controllers\UploadController@upload4');
    Route::post('/items-store', 'Tamis\Upload\Controllers\UploadController@storeArts2');
});
