<?php

namespace Tamis\Upload;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

/**
 * Class UploadServiceProvider
 *
 * @todo    publish/install dropzone.css, dropzone.js
 *
 * @package Tamis\Upload
 */
class UploadServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $router->aliasMiddleware('upload', \Tamis\Upload\Middleware\UploadMiddleware::class);
        
        $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');
//        $this->loadMigrationsFrom(__DIR__ . '/Migrations');
//        $this->loadTranslationsFrom(__DIR__ . '/Translations', 'upload');
        $this->loadViewsFrom(__DIR__ . '/Views', 'upload');

//        $this->publishes([__DIR__ . '/Config/upload.php' => config_path('upload.php'),], 'upload_config');
        $this->publishes([__DIR__ . '/Assets' => public_path(),], 'upload_assets');
//        $this->publishes(
//            [
//                __DIR__ . '/Translations' => resource_path('lang/vendor/upload'),
//                __DIR__ . '/Views'        => resource_path('views/vendor/upload'),
//            ]);

//        if ($this->app->runningInConsole()) {
//            $this->commands([
//                \Tamis\Upload\Commands\UploadCommand::class,
//            ]);
//        }
    }
    
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
//        $this->mergeConfigFrom(
//            __DIR__ . '/Config/upload.php', 'upload'
//        );
        $this->app->make('Tamis\Upload\Controllers\UploadController');
    }
}
